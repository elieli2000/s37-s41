const User = require('../models/users');
const Course = require('../models/course')
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true
		} else {
			return false 
		}

	})
};


module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false
		} else {
			return true
		}
	})
};

// loginUser function
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			// compareSync(<dataToCompare>, <encryopted password>)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		console.log(userData)
		if(result == null) {
			return false
		}  else {
				result.password= ""
				return result

			}
		
	})
};

module.exports.enroll = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId})

		return user.save().then((user, error) => {

			if(error){
				return false
			} else {
				return true
			}
		})
	})


let iscourseUpdated = await Course.findById(data.courseId).then(course => {

	course.enrollees.push({userId: data.userId})

	return course.save().then((course, error) => {

		if (error) {
			return false
		} else {
			return true
		}
	})
})


if (isUserUpdated && iscourseUpdated) {
	return "User successfully enrolled"
} else {
	return "User cannot enroll"
}

};

