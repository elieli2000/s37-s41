

const Course = require('../models/course');

const auth = require('../auth');


//



module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isadmin: reqBody.isAdmin
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return "Successfully added a Course"
		}
	})
};



module.exports.getAllCourses = (data) => {
	if(data.isAdmin) {
		return Course.find({}).then(result  => {
			return result
		})
	} else {
		return false
	}
};


module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
};

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {return result})
}

module.exports.updateCourse = (reqParams, reqBody, data) => {
	if(data) {
		let updatedCourse = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		}
		// Syntax: findByIdAndUpdate(document ID, updatesTobeApplied)
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	} else {
		return "Failed: You are not an Admin!"
	}
};


module.exports.archiveCourse = (reqParams, reqBody, data) => {
	if(data.isAdmin) {
		let updatedCourse = {
			isActive: false
		}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
	} else {
		return "Failed: Only an admin can do changes"
	}
}


