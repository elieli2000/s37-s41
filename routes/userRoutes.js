const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth')


// Routes:

// 

router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});


router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


router.get("/details", auth.verify, (req, res) => {
	
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController));



});

router.post('/enroll', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin){
		res.send("Failed: Admin cannot enroll")
	} else {
		let data = {
			userId: userData.id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
	}
})



module.exports = router;