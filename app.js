const express = require('express');
const mongoose = require('mongoose');

const cors = require('cors');

const userRoutes = require('./routes/userRoutes')

const courseRoutes= require('./routes/courseRoutes');

const app = express();
const port = process.env.PORT || 4000;


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({exteded: true}));


app.use("/users", userRoutes)
app.use("/courses", courseRoutes)
mongoose.connect(`mongodb+srv://admin:admin123@cluster0.evvkhyz.mongodb.net/S37-S41?retryWrites=true&w=majority`,{

	useNewUrlParser:true,
	useUnifiedTopology: true
});

const db = mongoose.connection

db.on('error', () => console.error('Connection Error'))
db.once('open', () => console.error('Connected to MongoDB'))


app.listen(port, () => console.log(`API is now online at port: ${port}`));